# HexBinMap

# Overview
This project is a data visualization of COVID-19 statistics in the United States for the month of July. It utilizes Leaflet.js to create an interactive map with hexbin representation to capture individual states' data.

# Technologies Used
Leaflet.js

# Features

- **Hexbin map visualization:** The map provides a hexbin representation, allowing for a clear view of COVID-19 statistics for individual states.
- **Interactive:** Users can interact with the map, zooming in/out and hovering over states to view detailed statistics.
- **Month Selection:** Currently focused on July, users may have the option to select different months for visualization, if implemented.

# How to Use
- Clone this repository to your local machine.
- Open index.html in your preferred web browser.
- Explore the interactive hexbin map to view COVID-19 statistics for individual states in the United States for the month of July.


# Credits
This project was created using Leaflet.js.
COVID-19 data source: https://www.nytimes.com/interactive/2021/us/covid-cases.html

# License
This project is licensed under the MIT License.
